# Import required modules
import csv
from re import T
import sqlite3
import os

global loaded_data
loaded_data = False

try:
    if os.path.exists('database.db'):
        connection = sqlite3.connect('database.db')
        cursor = connection.cursor()
        loaded_data = True
    else:
        loaded_data = False
except FileNotFoundError:
    loaded_data = False
    pass
except sqlite3.OperationalError:
    print("The Data has not been loaded")
    loaded_data = False

def load_data():
    """ Loads the dataset if not already on local machine

    """
    global cursor
    global loaded_data
    global connection
    #remove old database
    try:
        os.remove("database.db")
    except FileNotFoundError:
        pass
    
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    # Table Definition
    create_artist_table = '''CREATE TABLE artist(
                    pmkName TEXT PRIMARY KEY NOT NULL,
                    fldGender TEXT NOT NULL,
                    fldDescription TEXT NOT NULL,
                    fldCountry TEXT NOT NULL);
                    '''
                    
    create_song_table = '''CREATE TABLE song(
                    pmkRank INTEGER PRIMARY KEY AUTOINCREMENT,
                    fldTrack TEXT NOT NULL,
                    fpkName  TEXT NOT NULL,
                    fldGenre TEXT NOT NULL,
                    fldBPM INT NOT NULL,
                    fldLength INT NOT NULL);
                    '''
    
    # Creating the table into our
    # database

    cursor.execute(create_artist_table)
    cursor.execute(create_song_table)

    # Opening the person-records.csv file
    artists_file = open('artists.csv', encoding="utf8", errors="ignore")
    songs_file = open('top50.csv', encoding="utf8", errors="ignore")

    # Reading the contents of the
    # person-records.csv file
    artist_contents = csv.reader(artists_file)
    song_contents = csv.reader(songs_file)

    # SQL query to insert data into the
    # artist and song table
    insert_artist_records = "INSERT INTO artist (pmkName, fldGender, fldDescription, fldCountry) values (?, ?, ?, ?)"
    insert_song_records = "INSERT INTO song (fldTrack, fpkName, fldGenre, fldBPM, fldLength) values (?, ?, ?, ?, ?)"

    # Importing the contents of the file
    # into our person table

    cursor.executemany(insert_artist_records, artist_contents)
    cursor.executemany(insert_song_records, song_contents)

    # SQL query to retrieve all data from
    # the person table To verify that the
    # data of the csv file has been successfully
    # inserted into the table

    # Committing the changes
    connection.commit()

    print("Data loaded!")
    loaded_data = True


def convert_To_Fld(field):
    """ Converts keywords in laymans query to sql "database" query

    :param field: string query
    :type field: str

    """
    field = str.replace(field, "artist", "pmkName")
    field = str.replace(field, "gender", "fldGender")
    field = str.replace(field, "description", "fldDescription")
    field = str.replace(field, "country", "fldCountry")
    field = str.replace(field, "track", "fldTrack")
    field = str.replace(field, "genre", "fldGenre")
    field = str.replace(field, "bpm", "fldBPM")
    field = str.replace(field, "length", "fldLength")
    field = str.replace(field, "rank", "pmkRank")
    return field 


def process(column, condition, order):
    """ Converts laymans query to sql "database" query

    :param column: the database column name
    :type column: str
    :param condition: the conditional
    :type condition: str
    :param order: order in which the data is displayed
    :type order: str
    
    """
    sql_statement1 = "SELECT "  
    sql_statement2 = " FROM song JOIN artist on fpkName = pmkName "
    
    # check each of the english versions of the column name and
    # add the real column name to the SQL statement
    if (column == "artist"):
        sql_statement1 += "pmkName"
    elif (column == "gender"):
        sql_statement1 += "fldGender"
    elif (column == "description"):
        sql_statement1 += "fldDescription"
    elif (column == "country"):
        sql_statement1 += "fldCountry"
    elif (column == "track"):
        sql_statement1 += "fldTrack"
    elif (column == "genre"):
        sql_statement1 += "fldGenre"
    elif (column == "bpm"):
        sql_statement1 += "fldBPM"
    elif (column == "length"):
        sql_statement1 += "fldLength"
    elif (column == "rank"):
        sql_statement1 += "pmkRank"
    
    sql_statement = sql_statement1 + sql_statement2
    
    if (condition != ""):
        condition = str.replace(condition, " is greater than", " >")
        condition = str.replace(condition, " is less than ", " <")
        condition = str.replace(condition, " is greater than or equal to ", " >=")
        condition = str.replace(condition, " is less than or equal to ", " <=")
        condition = str.replace(condition, " is ", " =")
        condition = convert_To_Fld(condition)

        sql_statement += (condition + " ")
    
        if (order != ""):
            sql_statement3 = "ORDER BY "
            order = convert_To_Fld(order)
            sql_statement3 += order
            sql_statement += sql_statement3
        
        try:
            execute_SQL(sql_statement)
        except sqlite3.OperationalError:
            print("Invalid statement. Please type 'help' for a list of valid statements.")
            


def how_many(string):
    """ Gets number of rows
    
    :param string: user's query
    :type string: str
    
    """
    if (str.__contains__(string, "artists")):
        rows = cursor.execute("SELECT COUNT() as amount FROM artist")        
        numberOfrows = cursor.fetchone()[0]
        print(numberOfrows)
    elif (str.__contains__(string, "tracks")):
        rows = cursor.execute("SELECT COUNT() as amount FROM song")        
        numberOfrows = cursor.fetchone()[0]
        print(numberOfrows)


def execute_SQL(sql_statement):
    """ Executes the SQL statement

    :param sql_statement: the sql statement query
    :type sql_statement: str
    
    """
    output = []
    try:
        rows = cursor.execute(sql_statement).fetchall()
        
        if (rows[0] == ""):
            print("Invalid statement. Please type 'help' for a list of valid statements.")
        else:
            for r in rows:
                result = ""
                for l in r:
                    result += ", " + str(l)
                result = result[2:]
                result = str.lower(result)
                if result not in output:
                    print(result)
                output.append(result)
    except:
        if loaded_data:
            print("Invalid statement. Please type 'help' for a list of valid statements.")
        else:
            print("The Database has not been loaded")



def quit():
    # closing the database connection
    connection.close()
