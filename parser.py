HELP_KEYWORD = "help"
ORDER_BY_KEYWORD = "order by"
HOW_MANY = "how many"


def print_msg_box(msg, indent=2, width=None, title=None):
    """Print message-box with optional title.
    
    :param msg: help message
    :type msg: str
    :param indent: indent size
    :type indent: int
    :param width: width size
    :type width: int
    :param title: message title
    :type title: str

    """
    lines = msg.split('\n')
    space = " " * indent
    if not width:
        width = max(map(len, lines))
    box = f'╔{"═" * (width + indent * 2)}╗\n'  # upper_border
    if title:
        box += f'║{space}{title:<{width}}{space}║\n'  # title
        box += f'║{space}{"-" * len(title):<{width}}{space}║\n'  # underscore
    box += ''.join([f'║{space}{line:<{width}}{space}║\n' for line in lines])
    box += f'╚{"═" * (width + indent * 2)}╝'  # lower_border
    print(box)

def help() -> str:
    """ Prints help instructions: format, query structure etc.

    """

    string_test = '\nMake sure you loaded the data by typing load\n\n' \
        'Format your search: WANT where ITEM is "NAME/COMPARATOR"\n\n' \
        'Make sure to put quotes around the name/comparator (e.g. "dance monkey" or "drake) in quotes.\n\n' \
        'Optional: add order by IDENTIFIER to display results in a specific order \n\n' \
        'Optional: add greater than or less than to get a range of results for integer identifiers\n\n' \
        'Optional: to get the total columns type how many followed by artists or tracks\n\n' \
        'Example Query 1: artist where track is "dance monkey"\n\n'  \
        'Example Query 2: track where artist is "ed sheeran" order by length \n\n' \
        'Example Query 3: track where bpm is greater than 110 order by rank \n\n' \
        'Example Query 4: how many artists'

    print_msg_box(string_test, title="HELP:")


def parser(user_input) -> str:
    """ Parse and format the user query for the query search

    :param user_input: the user input
    :type user_input: str

    """
    user_input = user_input.lower()

    # if the user would like to know the total number of elemenets in either list
    if HOW_MANY in user_input:
        return user_input
        
    else:
        # split the string on the first white space to seperate column name
        user_input_split = user_input.split(" ", 1)

        # if "order by" is in the second half of the user input split on "order by" to get the order
        if "order by" in user_input_split[1]:

            user_input_split1 = user_input_split[1].split(" order by ")  

            return(user_input_split[0], user_input_split1[0], user_input_split1[1])

        # else return a blank order
        else:

            return user_input_split[0], user_input_split[1], ""
