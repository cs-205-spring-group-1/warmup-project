# Warmup Collaborative Project: command-line interface

## Description

Hi! This is a python command-line interface to query from the dataset *Top 50 Spotify songs of 2019* stored in a relational database.

## Datasets Used
- [Top 50 Spotify Spotify Songs - 2019](/top50.csv)

- [Spotify Artist Metadata Top 10k](/artists.csv)

## Key functions & query words/phrases

<details><summary><b>Functions (click me) </b></summary>
<p>

- Load
- Help
- Exit

</p>
</details>


<details><summary><b>Conditionals</b></summary>
<p>

- How many
- Where (identifier)
- Order By
- Greater than
- Less than
- Greater than or equal to
- Less than or equal to
- is

</p>
</details>

<details><summary><b>Identifiers</b></summary>
<p>

- Rank
- Track
- Artist
- Gender
- Group
- Country
- Genre
- BPM
- Length

</p>
</details>

### **Example User Queries To Try!**
#### Format: WANT where ITEM is “NAME/COMPARATOR"

- artist where track is “dance monkey”

- track where artist is "ed sheeran" order by length

- track where bpm is greater than 110 order by rank