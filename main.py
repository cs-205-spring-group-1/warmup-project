"""
This is the python script to control the terminal for our database specific Query Language

Started by Dylan Lawrence on 2/2/2022
"""

from parser import help, parser
from sql_functions import process, load_data, quit, how_many

#gets user input
def get_user_input() -> str:
    return input(">")

def main():
    quit_loop = False

    #Main loop
    while quit_loop == False:
        #get input
        user_input = get_user_input().lower().strip()

        #handle potential edge cases
        if user_input == 'help':
            help()
            continue
        
        elif user_input == 'quit':
            quit_loop = True
            quit()
            continue
        
        elif user_input == 'load data':
            load_data()
            continue

        #else try to parse input, if invalid statement print an error message
        try:
            parsed_input = parser(user_input)
            if parsed_input == None:
                print("Invalid statement. Please type 'help' for a list of valid statements.")
            elif ("tracks" in user_input):
                how_many("tracks")
            elif ("artists" in user_input):
                how_many("artists")
            else:
                process(parsed_input[0], parsed_input[1], parsed_input[2])
        except:
            print("Invalid statement. Please type 'help' for a list of valid statements.")

main()
